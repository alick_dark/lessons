package packDB;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/logIn")
public class LogINServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LogInServices logInServices = new LogInServices();
        HttpSession session = request.getSession();

        try {
            logInServices.main(request.getParameter("login"), request.getParameter("password"));
            session.setAttribute("logInClass", logInServices);
            request.setAttribute("friendList", logInServices.getFriendList());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //request.setAttribute("error", logInClass.getError());
        response.sendRedirect("/home");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/logIn.jsp");
        dispatcher.forward(request, response);

    }
}
