package packDB;

import java.sql.*;
import java.util.LinkedList;

public class LogInServices {

    Integer idUsers;
    Integer idFriend;
    public String friendName;
    public String name;
    public String info;
    public String date;
    public String job;
    public String error = "";
    LinkedList<String> friendList = new LinkedList<String>();

    public void main (String login, String pass) throws ClassNotFoundException, SQLException {

        Connection connection;

        String serverName = "localhost";
        String baseName = "socialnet";
        String url = "jdbc:mysql://" + serverName + "/" + baseName;
        String username = "root";
        String password = "lancer69";
        Class.forName("com.mysql.jdbc.Driver"); // назва драйвера
        connection = DriverManager.getConnection(url, username, password);

        Statement state = connection.createStatement();
        Statement statement = connection.createStatement();
        Statement statement1 = connection.createStatement();

        String queryToUsers = "Select * From users Where Login = '" + login + "'";

        ResultSet resultSet = state.executeQuery(queryToUsers);

        while (resultSet.next()){
            if (pass.equals(resultSet.getString(3))){
                idUsers = resultSet.getInt(1);
                name = resultSet.getString(4);
                info = resultSet.getString(5);
                date = resultSet.getString(6);
                job = resultSet.getString(7);
            }else {
                error = "You input incorrect login or password";
            }
        }

        String queryToRelation = "Select * From relation Where id_user = '" + idUsers +"'";

        ResultSet resultSet1 = statement.executeQuery(queryToRelation);

        while (resultSet1.next()){
            idFriend = resultSet1.getInt(2);
        }
        System.out.println(idFriend.toString());

        String queryToGetFriend = "Select Name From users Where id_users = '" + idFriend + "'";

        ResultSet resultSet2 = statement1.executeQuery(queryToGetFriend);

        while (resultSet2.next()){
            friendName = resultSet2.getString(1);
            friendList.add(friendName);
        }
        for (int i = 0; i < friendList.size(); i ++){
            System.out.println("\n" + friendList.get(i));
        }
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public Integer getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(Integer idUsers) {
        this.idUsers = idUsers;
    }

    public Integer getIdFriend() {
        return idFriend;
    }

    public void setIdFriend(Integer idFriend) {
        this.idFriend = idFriend;
    }

    public LinkedList<String> getFriendList() {
        return friendList;
    }

    public void setFriendList(LinkedList<String> friendList) {
        this.friendList = friendList;
    }
}
