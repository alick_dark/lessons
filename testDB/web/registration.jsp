<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>

    <H1 align = "center">Please input required information:</H1><br>
    <form id="registration" method="post" action="/registration">
        <table align = "center">
            <tr>
                <td>Login:</td>
                <td><input type="text" name="login"/></td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><input type="password" name="password"/></td>
            </tr>
            <tr>
                <td>Your real name and surname:</td>
                <td><input type="text" name="name"/></td>
            <tr>
                <td>Information about you:</td>
                <td><textarea form="registration" cols="20" rows="10" name="info"></textarea></td>
            </tr>
            <tr>
                <td>Date of your birthday:</td>
                <td><input type="date" name="date"/></td>
            </tr>
            <tr>
                <td>Where your occupation:</td>
                <td><input type="text" name="job"></td>
            </tr>
            <tr>
                <td>E-mail:</td>
                <td><input type="text" name="email"></td>
            </tr>
            <tr>
                <td>Phone number:</td>
                <td><input type="text" name="phone"></td>
            </tr>
            <tr>
                <td><input type="submit" value="Registrate"></td>
            </tr>
        </table>
    </form>
</body>
</html>
