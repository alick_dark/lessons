package pack;

public class Boat implements Vehicle {

    Boolean engineWorks;
    String accelerate = "You move";
    String brake = "You brake";
    String turnLeft = "You rudder turned lefrt";
    String turnRight = "You rudder turned right";
    String request = "Please start motor";

    @Override
    public String accelerate() {
        if (engineWorks == true){
            return accelerate;
        }else return request;
    }

    @Override
    public String brake() {
        if (accelerate.equals("You move")){
        return brake;
        }else return "You stand in place";
    }

    @Override
    public String turnLeft() {
        return turnLeft;
    }

    @Override
    public String turnRight() {
        return turnRight;
    }


    @Override
    public Boolean ignition() {
        engineWorks = true;
        return engineWorks;
    }

    @Override
    public String request() {
        return request;
    }
}
