package pack;

import javax.swing.text.html.parser.DTD;

public class Car implements Vehicle {

    Boolean engineWorks;
    String accelerate = "You move";
    String brake = "You brake";
    String turnLeft = "You rudder turned lefrt";
    String turnRight = "You rudder turned right";
    String request = "Please start motor";

    @Override
    public String accelerate() {
        if (engineWorks == true){
        return accelerate;
        } else return request;
    }

    @Override
    public String brake() {
        if (engineWorks == true){
        return brake;
        } else return "You stand in place";
    }

    @Override
    public String turnLeft() {
        return turnLeft;
    }

    @Override
    public String turnRight() {
        return turnRight;
    }


    @Override
    public Boolean ignition(){
        engineWorks = true;
        return engineWorks;
    }
    @Override
    public String request(){
        return request;
    }
}
