package pack;

public interface Vehicle {
    public String accelerate();
    public String brake();
    public String turnLeft();
    public String turnRight();
    public Boolean ignition();
    public String request();
}
