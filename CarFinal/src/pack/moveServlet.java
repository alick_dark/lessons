package pack;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

@WebServlet("/driveVehicle")
public class moveServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Vehicle vehicle = (Vehicle) session.getAttribute("vehicle");
        String version = (String) request.getParameter("vehicle");

        if (version.equals("accelerate")){
            vehicle.accelerate();
        } else if (version.equals("brake")){
            vehicle.brake();
        }else if(version.equals("turnLeft")){
            vehicle.turnLeft();
        }else vehicle.turnRight();

        session.setAttribute("choose", vehicle);
        response.sendRedirect("/driveVehicle");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/move.jsp");
        dispatcher.forward(request, response);
    }
}
