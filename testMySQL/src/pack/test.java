package pack;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class test {
    public static void main (String[] args) throws SQLException {

        Connection connection;

        String driverName = "com.mysql.jdbc.Driver"; // назва драйвера

            // створюєм конекшн
        String serverName = "localhost";
        String baseName = "test";
        String url = "jdbc:mysql://" + serverName + "/" + baseName;
        String username = "root";
        String password = "";

        connection = DriverManager.getConnection(url, username, password);
        System.out.println("is conect to DB" + connection);

        Statement state  = connection.createStatement();

        // state.executeUpdate("INSERT into test.users (Login, Password, Name, Info, DateOfBirthday, Job) VALUES ('login', 'pass', 'Alick', 'info', '1994.12.19', 'styd')");

        String query = "Select max(id_users) From users";

        ResultSet rs = state.executeQuery(query);

        int id = 0;
        while (rs.next()) {
            id = rs.getInt(1);
        }

        id++;

        state.executeUpdate("INSERT into test.users (Login, Password, Name, Info, DateOfBirthday, Job) VALUES ('login', 'pass', 'Alick', 'info', '1994.12.19', 'styd')");
    }
}
